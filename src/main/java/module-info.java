module com.mycompany.javafxtp1 {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.base;

    opens com.mycompany.javafxtp1 to javafx.fxml;
    exports com.mycompany.javafxtp1;
}