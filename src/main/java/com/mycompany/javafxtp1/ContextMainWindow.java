/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javafxtp1;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author colet
 */
public class ContextMainWindow 
{
    private Users users;
    private User connectedUser;
    
    public ContextMainWindow(Users users, User connectedUser)
    {
        this.users = users;
        this.connectedUser = connectedUser;
    }
    
    public Users getUsers()
    {
        return users;
    }
    
    public User getConnectedUser()
    {
        return connectedUser;
    }
}
