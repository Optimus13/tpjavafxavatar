/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javafxtp1;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ButtonType;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
/**
 * FXML Controller class
 *
 * @author colet
 */
public class MainWindowController implements Initializable 
{
    private ContextMainWindow context;
    @FXML
    private TableView<User> tableView;
    @FXML
    private TableColumn<User, String> login;
    @FXML
    private TableColumn<User, String> name;
    @FXML
    private TextField loginField;
    @FXML
    private TextField passwordField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField cityField;
    @FXML
    private Button deleteUserProcess;
    @FXML
    private TableColumn<User, String> city;
    
    private User selectedUser;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
        tableView.getSelectionModel().selectedItemProperty().addListener((observable, oldSelect, newSelect) -> 
        {
            LineSelected(newSelect, oldSelect);
        });
    }    
    
    public void setContext(ContextMainWindow context)
    {
        this.context = context;
        
        tableView.getItems().addAll(context.getUsers().getList());
        login.setCellValueFactory(new PropertyValueFactory<>("login"));
        name.setCellValueFactory(new PropertyValueFactory<>("name"));
        city.setCellValueFactory(new PropertyValueFactory<>("villeCP"));
    }
    
    public void LineSelected(User new_u, User old_u)
    {
        if(old_u != null)
        {
            old_u.loginProperty().unbindBidirectional(loginField.textProperty());
            old_u.passwordProperty().unbindBidirectional(passwordField.textProperty());
            old_u.nameProperty().unbindBidirectional(nameField.textProperty());
            old_u.villeCPProperty().unbindBidirectional(cityField.textProperty());
        }        
        
        if(new_u != null)
        {
            selectedUser = new_u;
        
            loginField.setText(new_u.getLogin());
            passwordField.setText(new_u.getPassword());
            nameField.setText(new_u.getName());
            cityField.setText(new_u.getVilleCP());  

            new_u.loginProperty().bindBidirectional(loginField.textProperty());
            new_u.passwordProperty().bindBidirectional(passwordField.textProperty());
            new_u.nameProperty().bindBidirectional(nameField.textProperty());
            new_u.villeCPProperty().bindBidirectional(cityField.textProperty()); 
        } 
    }
    
    @FXML
    private void profileProcess(ActionEvent event) 
    {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("User Information");
        alert.setHeaderText(context.getConnectedUser().getLogin() + " Information");
        alert.setContentText("Name: " + context.getConnectedUser().getName() +
                             "\nAddress: " + context.getConnectedUser().getAddress1() + 
                             "\nTelephone: " + context.getConnectedUser().getTel1());
        
        alert.showAndWait();
    }

    @FXML
    private void avatarProcess(ActionEvent event) 
    {
        try {
            Stage stage = new Stage();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(App.class.getResource("AvatarCustomizer.fxml"));
            BorderPane rootLayout = (BorderPane) loader.load();

            ContextUser context = new ContextUser(this.context.getConnectedUser());
            AvatarCustomizerController controller = loader.getController();
            controller.setContext(context);

            Scene scene = new Scene(rootLayout);
            stage.setScene(scene);
            stage.centerOnScreen();
            stage.setResizable(false);
            stage.show();
        } 
        catch (IOException e) 
        {
            e.printStackTrace();
            //Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    
    
    @FXML
    private void quitProcess(ActionEvent event) 
    {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Are you ok to quit this wonderful application?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            Platform.exit();
            System.exit(0);
        } 
        else 
        {
            // ... user chose CANCEL or closed the dialog
        }
    }

    @FXML
    private void addUserProcess(ActionEvent event) 
    {
        context.getUsers().getList().add(new User("Replace Me", "Replace Me", "Replace Me", "address1", "address2", "Replace Me", "tel1", "tel2"));
        RefreshTableView();
    }

     @FXML
    private void deleteUserProcess(ActionEvent event) 
    {
        if (selectedUser != null) {
            context.getUsers().getList().remove(selectedUser);
        }
        RefreshTableView();
    }
    
    public void RefreshTableView()
    {
        tableView.getItems().clear();
        tableView.getItems().addAll(context.getUsers().getList());
    }
    
}
