/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javafxtp1;

import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author colet
 */
public class Users 
{
    private final ObservableList<User> list;
    
    public Users(List<User> list)
    {
        this.list = FXCollections.observableArrayList(list);
    }
    
    public Users()
    {
        this.list = FXCollections.observableArrayList();
        this.list.add(new User("login", "password1!", "user", "address1", "address2", "villeCP", "tel1", "tel2"));
        this.list.add(new User("toto", "password1!", "toto", "address1", "address2", "toulouse", "tel1", "tel2"));

    }

    public ObservableList<User> getList() {
        return list;
    }
    
    public User identification(String login, String password)
    {
        for (User u: list)
        {
            if(u.CheckPassword(login, password))
            {
                return u;
            }
        }
        return null;
    }
}
