/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javafxtp1;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Slider;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;

/**
 * FXML Controller class
 *
 * @author colet
 */
public class AvatarCustomizerController implements Initializable {

    @FXML
    private Slider hairSlider;
    @FXML
    private ListView<String> faceList;
    @FXML
    private ComboBox<String> eyePicker;
    @FXML
    private Canvas canvas;
    
    ContextUser context;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {       
        //Face Shape
        ObservableList<String> faces = FXCollections.observableArrayList("Round", "Oval", "Square");
        faceList.setItems(faces);
        faceList.selectionModelProperty().get().setSelectionMode(SelectionMode.SINGLE);
        
        //Initialization eyesPicker
        ObservableList<String> eyes = FXCollections.observableArrayList ("Red", "Blue", "Green");
        eyePicker.setItems(eyes);
        
        faceList.selectionModelProperty().get().select("Round");
        eyePicker.selectionModelProperty().get().select("Blue");
        hairSlider.setValue(0);
    }   
    
    public void setContext(ContextUser context)
    {
        this.context = context;
        context.getUser().HairLengthProperty().bindBidirectional(hairSlider.valueProperty());
        context.getUser().FaceShapeProperty().bind(faceList.getSelectionModel().selectedItemProperty());
        context.getUser().EyeColorProperty().bindBidirectional(eyePicker.valueProperty());
        
        //Hair Slider
        hairSlider.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            Draw();
        });
        
        faceList.selectionModelProperty().get().getSelectedItems().addListener((ListChangeListener.Change<? extends String> c) -> {
            Draw();
        });
        
        eyePicker.valueProperty().addListener((ObservableValue<? extends String> ov, String oldValue, String newValue) -> {
            Draw();
        });        
        
        Draw();
    }
    
    public void DrawFace(String face)
    {
        System.out.println(face);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.BEIGE);
        
        switch(face)
        {
            case "Round":
                gc.fillOval(canvas.getWidth()/2.0f-50, canvas.getHeight()/2.0f-50, 100, 100);
                break;
            case "Oval":
                gc.fillOval(canvas.getWidth()/2.0f-50, canvas.getHeight()/2.0f-75, 100, 150);
                break;
            case "Square":
                gc.fillRect(canvas.getWidth()/2.0f-50, canvas.getHeight()/2.0f-50, 100, 100);
                break;
            default:
                break;                
        }   
        
        //Mouth
        gc.setFill(Color.WHITE);
        gc.fillArc(canvas.getWidth()/2.0f-30, canvas.getHeight()/2.0f, 60, 30, 180, 180, ArcType.OPEN);
    }
    
    public void DrawEye(String eye)
    {
        System.out.println(eye);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        
        switch(eye)
        {
            case "Red":
                gc.setFill(Color.RED);
                gc.fillOval(canvas.getWidth()/2.0f+15, canvas.getHeight()/2.0f-20, 10, 10);
                gc.fillOval(canvas.getWidth()/2.0f-25, canvas.getHeight()/2.0f-20, 10, 10);
                break;
            case "Blue":
                gc.setFill(Color.BLUE);
                gc.fillOval(canvas.getWidth()/2.0f+15, canvas.getHeight()/2.0f-20, 10, 10);
                gc.fillOval(canvas.getWidth()/2.0f-25, canvas.getHeight()/2.0f-20, 10, 10);
                break;
            case "Green":
                gc.setFill(Color.GREEN);
                gc.fillOval(canvas.getWidth()/2.0f+15, canvas.getHeight()/2.0f-20, 10, 10);
                gc.fillOval(canvas.getWidth()/2.0f-25, canvas.getHeight()/2.0f-20, 10, 10);
                break;
            default:
                break;                
        }        
    }
    
    /*public void DrawHair(float length)
    {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.BLACK);
        gc.strokeLine(canvas.getWidth()/2.0f, canvas.getHeight()/2.0f-50, canvas.getWidth()/2.0f, canvas.getHeight()/2.0f-50-length*2);
        gc.strokeLine(canvas.getWidth()/2.0f-12, canvas.getHeight()/2.0f-50, canvas.getWidth()/2.0f-12, canvas.getHeight()/2.0f-50-length*2);
        gc.strokeLine(canvas.getWidth()/2.0f+12, canvas.getHeight()/2.0f-50, canvas.getWidth()/2.0f+12, canvas.getHeight()/2.0f-50-length*2);
        gc.strokeLine(canvas.getWidth()/2.0f-25, canvas.getHeight()/2.0f-50, canvas.getWidth()/2.0f-25, canvas.getHeight()/2.0f-50-length*2);
        gc.strokeLine(canvas.getWidth()/2.0f+25, canvas.getHeight()/2.0f-50, canvas.getWidth()/2.0f+25, canvas.getHeight()/2.0f-50-length*2);
        System.out.println(length);
    }*/
    
    public void DrawHair(float length)
    {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        System.out.println(length);
        gc.strokeArc(100, 150, 30, length*20 , 90, 45 , ArcType.OPEN);
        gc.strokeArc(105, 145, 30, length*22 , 90, 45 , ArcType.OPEN);
        gc.strokeArc(110, 140, 30, length*24 , 70, 45 , ArcType.OPEN);
        gc.strokeArc(115, 135, 30, length*26 , 70, 45 , ArcType.OPEN);
        gc.strokeArc(120, 130, 30, length*28 , 70, 45 , ArcType.OPEN);
        gc.strokeArc(150, 130, 30, length*28 , 65, 45 , ArcType.OPEN);
        gc.strokeArc(155, 135, 30, length*26 , 65, 45 , ArcType.OPEN);
        gc.strokeArc(160, 140, 30, length*24 , 65, 45 , ArcType.OPEN);
        gc.strokeArc(165, 145, 30, length*22 , 45, 45 , ArcType.OPEN);
        gc.strokeArc(170, 150, 30, length*20 , 45, 45 , ArcType.OPEN);
    }
    
    public void Draw()
    {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.clearRect(0,0, canvas.getWidth(), canvas.getHeight());
        DrawFace(context.getUser().getFaceShape());
        DrawEye(context.getUser().getEyeColor());
        DrawHair(context.getUser().getHairLength());
    }

    @FXML
    private void reset(ActionEvent event) 
    {
        faceList.selectionModelProperty().get().select("Round");
        eyePicker.selectionModelProperty().get().select("Blue");
        hairSlider.setValue(0);
        Draw();
    }    
}
