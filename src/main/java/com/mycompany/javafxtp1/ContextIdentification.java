/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javafxtp1;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author colet
 */
public class ContextIdentification 
{
    private Users users;
    private StringProperty loginConnectedUser;
    private StringProperty passwordConnectedUser;
    
    public ContextIdentification(Users users)
    {
        this.users = users;
        loginConnectedUser = new SimpleStringProperty();
        passwordConnectedUser = new SimpleStringProperty();
    }
    
    public Users getUsers()
    {
        return users;
    }
    
    public StringProperty loginConnectedUserProperty()
    {
        return loginConnectedUser;
    }
    
    public String getLoginConnectedUser()
    {
        return loginConnectedUser.getValue();
    }
    
    public StringProperty passwordConnectedUserProperty()
    {
        return passwordConnectedUser;
    }
    
    public String getPasswordConnectedUser()
    {
        return passwordConnectedUser.getValue();
    }
    
    public User identification()
    {
        return users.identification(loginConnectedUser.getValue(), passwordConnectedUser.getValue());
    }
}
