/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javafxtp1;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author colet
 */
public class LoginController implements Initializable {

    @FXML
    private TextField login;
    @FXML
    private PasswordField password;
    @FXML
    private Button ok;
    @FXML
    private Button cancel;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private Label passwordComment;
    
    ContextIdentification context;    
    
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
        ButtonBar.setButtonData(ok, ButtonBar.ButtonData.OK_DONE);
        ButtonBar.setButtonData(cancel, ButtonBar.ButtonData.CANCEL_CLOSE);
        
        /*password.textProperty().addListener(new ChangeListener<String>() 
        {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue)
            {
                progressBar.setProgress(newValue.length()/8.0f);
            }
        });*/
        
        /*password.textProperty().addListener((observable, oldValue, newValue) -> 
        {
            progressBar.setProgress(newValue.length()/8.0f);
        });*/
        
        //Progress Bar
        progressBar.progressProperty().bind(password.textProperty().length().add(login.textProperty().length()).divide(16.0f));
        
        //Check password composition
        password.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> 
        {
            String output = "Your password needs at least a number and a non-alphanumerical character.";
            boolean digit = false, alpha = false;
            if(newValue.length() == 0)
                passwordComment.textProperty().setValue("");
            else
            {
                for(char c: newValue.toCharArray())
                {
                    if(Character.isDigit(c))
                    {
                        digit = true;
                    }    
                    
                    if(!Character.isLetterOrDigit(c))
                    {
                        alpha = true;
                    }
                }
                
                if(digit && !alpha)
                    output = "Your password needs at least a non-alphanumerical character.";
                else if (!digit && alpha)
                    output = "Your password needs at least a number.";
                else if (digit && alpha)
                    output = "";
                
                passwordComment.textProperty().setValue(output);
            }
        });
        
        Platform.runLater(new Runnable() 
        {
            public void run() 
            {
                login.requestFocus();
            }
        });
    }   
    
    public void setContext(ContextIdentification context)
    {
        this.context = context;
        context.loginConnectedUserProperty().bind(login.textProperty());
        context.passwordConnectedUserProperty().bind(password.textProperty());
    }

    @FXML
    private void processCancel(ActionEvent event) 
    {
        login.textProperty().set("");
        password.textProperty().set("");
    }

    @FXML
    private void processOK(ActionEvent event) 
    {
        System.out.println("Login: " + context.loginConnectedUserProperty().getValue() + 
                            "\nPassword: " + context.passwordConnectedUserProperty().getValue());
        User connectedUser = context.identification();
        if(connectedUser != null)
        {
            try
            {
                Stage stage = (Stage)this.login.getScene().getWindow();
                
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(App.class.getResource("MainWindow.fxml"));
                BorderPane rootLayout = (BorderPane)loader.load();

                ContextMainWindow context = new ContextMainWindow(this.context.getUsers(), connectedUser);
                MainWindowController controller = loader.getController();
                controller.setContext(context);

                Scene scene = new Scene(rootLayout);
                stage.setResizable(true);
                stage.setScene(scene);
                stage.centerOnScreen();
            }
            catch(IOException e)
            {
                e.printStackTrace();
                //Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }
    
    @FXML
    private void processOKMouseEntered(MouseEvent event) 
    {
        if(login.textProperty().getValue().equals(""))
            login.setStyle("-fx-control-inner-background: red");        
    }

    @FXML
    private void processOKMouseExited(MouseEvent event) 
    {
        login.setStyle("-fx-control-inner-background: white");
    }    
}
