/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javafxtp1;

import java.time.LocalDateTime;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author colet
 */
public class User 
{
    private final StringProperty login = new SimpleStringProperty();
    private final StringProperty password = new SimpleStringProperty();
    private final StringProperty name = new SimpleStringProperty();
    private final StringProperty address1 = new SimpleStringProperty();
    private final StringProperty address2 = new SimpleStringProperty();
    private final StringProperty villeCP = new SimpleStringProperty();
    private final StringProperty tel1 = new SimpleStringProperty();
    private final StringProperty tel2 = new SimpleStringProperty();
    
    private final FloatProperty hairLength = new SimpleFloatProperty();
    private final StringProperty faceShape = new SimpleStringProperty();
    private final StringProperty eyeColor = new SimpleStringProperty();
    
    private final ObjectProperty<LocalDateTime> lastConnection = new SimpleObjectProperty();
    
    public User(String login, String password, String name, String address1, String address2, String villeCP, String tel1, String tel2)
    {
        this.login.set(login);
        this.password.set(password);
        this.name.set(name);
        this.address1.set(address1);
        this.address2.set(address2);
        this.villeCP.set(villeCP);
        this.tel1.set(tel1);
        this.tel2.set(tel2);
        
        this.faceShape.set("Round");
        this.eyeColor.set("Blue");
        this.hairLength.set(0.0f);
    }
    
    public boolean CheckPassword(String login, String password)
    {
        return login.equals(this.login.getValue()) && password.equals(this.password.getValue());
    }

    public final String getLogin() {
        return login.get();
    }

    public final void setLogin(String value) {
        login.set(value);
    }

    public StringProperty loginProperty() {
        return login;
    }

    public final String getPassword() {
        return password.get();
    }

    public final void setPassword(String value) {
        password.set(value);
    }

    public StringProperty passwordProperty() {
        return password;
    }

    public final String getName() {
        return name.get();
    }

    public final void setName(String value) {
        name.set(value);
    }

    public StringProperty nameProperty() {
        return name;
    }

    public final String getAddress1() {
        return address1.get();
    }

    public final void setAddress1(String value) {
        address1.set(value);
    }

    public StringProperty address1Property() {
        return address1;
    }

    public final String getAddress2() {
        return address2.get();
    }

    public final void setAddress2(String value) {
        address2.set(value);
    }

    public StringProperty address2Property() {
        return address2;
    }

    public final String getVilleCP() {
        return villeCP.get();
    }

    public final void setVilleCP(String value) {
        villeCP.set(value);
    }

    public StringProperty villeCPProperty() {
        return villeCP;
    }    
    
    public final String getTel1() {
        return tel1.get();
    }

    public final void setTel1(String value) {
        tel1.set(value);
    }

    public StringProperty tel1Property() {
        return tel1;
    }

    public final String getTel2() {
        return tel2.get();
    }

    public final void setTel2(String value) {
        tel2.set(value);
    }

    public StringProperty tel2Property() {
        return tel2;
    }    

    public FloatProperty HairLengthProperty() {
        return hairLength;
    }
    
    public final float getHairLength() {
        return hairLength.get();
    }
    
    public final void setHairLength(float value) {
        hairLength.set(value);
    }

    public StringProperty FaceShapeProperty() {
        return faceShape;
    }
    
    public final String getFaceShape() {
        return faceShape.get();
    }
    
    public final void setFaceShape(String value) {
        faceShape.set(value);
    }

    public StringProperty EyeColorProperty() {
        return eyeColor;
    }
    
    public final String getEyeColor() {
        return eyeColor.get();
    }
    
    public final void setEyeColor(String value) {
        eyeColor.set(value);
    }
    
    public final LocalDateTime getLastConnection() {
        return lastConnection.get();
    }

    public final void setLastConnection(LocalDateTime value) {
        lastConnection.set(value);
    }

    public ObjectProperty<LocalDateTime> lastConnectionProperty() {
        return lastConnection;
    }
}
