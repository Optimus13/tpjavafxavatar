/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javafxtp1;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 *
 * @author thomas colette
 */
public class App extends Application
{
    @Override
    public void start(Stage primaryStage)
    {
        try
        {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(App.class.getResource("Login.fxml"));
            BorderPane rootLayout = (BorderPane)loader.load();
            
            LoginController controller = loader.getController();
            ContextIdentification context = new ContextIdentification(new Users());
            controller.setContext(context);
            
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            
            scene.getStylesheets().add(LoginController.class.getResource("LoginCSS.css").toExternalForm());
            
            primaryStage.setResizable(false);
            
            primaryStage.show();            
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args)
    {
        launch(args);
    }
}
