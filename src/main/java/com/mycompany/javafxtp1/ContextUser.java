/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javafxtp1;

/**
 *
 * @author colet
 */
public class ContextUser
{
    private User user;
    
    public ContextUser(User user)
    {
        this.user = user;
    }
    
    public User getUser()
    {
        return user;
    }
}
